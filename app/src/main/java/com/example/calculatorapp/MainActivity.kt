package com.example.calculatorapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculatorapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.apply {
            btnLinearLayout.setOnClickListener {
                supportFragmentManager
                    .beginTransaction()
                    .add(android.R.id.content, LinearLayoutFragment())
                    .addToBackStack(null)
                    .commit()
            }
            btnFrameLayout.setOnClickListener {
                supportFragmentManager
                    .beginTransaction()
                    .add(android.R.id.content, FrameLayoutFragment())
                    .addToBackStack(null)
                    .commit()
            }
            btnConstraintLayout.setOnClickListener {
                supportFragmentManager
                    .beginTransaction()
                    .add(android.R.id.content, ConstraintLayoutFragment())
                    .addToBackStack(null)
                    .commit()
            }
        }
        setContentView(binding.root)
    }
}