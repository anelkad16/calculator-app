package com.example.calculatorapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.calculatorapp.databinding.FragmentConstraintLayoutBinding
import com.example.calculatorapp.databinding.FragmentLinearLayoutBinding

class ConstraintLayoutFragment : Fragment() {

    private lateinit var binding: FragmentConstraintLayoutBinding
    private var textSequence: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentConstraintLayoutBinding.inflate(inflater, container, false)
        with(binding) {
            btnMinus.setOnClickListener { addToTextField("-") }
            btnMultiply.setOnClickListener { addToTextField("*") }
            btnPlus.setOnClickListener { addToTextField("+") }
            valueOne.setOnClickListener { addToTextField("1") }
            valueTwo.setOnClickListener { addToTextField("2") }
            valueThree.setOnClickListener { addToTextField("3") }
            valueFour.setOnClickListener { addToTextField("4") }
            valueFive.setOnClickListener { addToTextField("5") }
            valueSix.setOnClickListener { addToTextField("6") }
            valueSeven.setOnClickListener { addToTextField("7") }
            valueEight.setOnClickListener { addToTextField("8") }
            valueNine.setOnClickListener { addToTextField("9") }
        }
        return binding.root
    }

    private fun addToTextField(text: String) {
        textSequence += text
        binding.editText.setText(textSequence)
    }

}